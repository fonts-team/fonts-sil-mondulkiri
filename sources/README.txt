sources/README.txt
Mondulkiri 7.100
==================

This file describes the VOLT source files included with the Mondulkiri
font family. This information should be distributed along with the Mondulkiri
fonts and any derivative works.

These files are part of Mondulkiri font family 
(http://scripts.sil.org/Mondulkiri) and are 
Copyright (c) 2003-2014 SIL International (http://www.sil.org/),
with Reserved Font Names "Mondulkiri", "Busra", "Oureang" and "Ratanakiri".

This Font Software is licensed under the SIL Open Font License,
Version 1.1.

You should have received a copy of the license along with this Font Software.
If this is not the case, go to (http://scripts.sil.org/OFL) for all the
details including an FAQ.

The italics fonts use the same VOLT and 'add' tables as the corresponding non-italics fonts.

kMon40V7E7.vtp for Mondulkiri-R/I
kMon60V7E7.vtp for Mondulkiri-B/BI
kMon50V7E7.vtp for Busra-R/I
kMon80V7E7.vtp for Busra-B/BI

All Mondulkiri fonts use the same 'add' file (khmer4add.xml).

Busra-R and Busra-I use khmer5add.xml, Busra-B and Busra-BI use khmer8add.xml.

All fonts use the same 'mif' file.

